package com.osagie.careemmoviedb.network;

import com.osagie.careemmoviedb.model.MoviesWrapper;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface ServiceGenerator {

    //--GET REQUEST---------------------------------------------------------------------------------
    @GET("discover/movie")
    Observable<MoviesWrapper> getMovies(@Query("sort_by") String sort_type,
                                        @Query("api_key") String api_key,
                                        @Query("page") int page,
                                        @Query("primary_release_date.gte") String fromDate,
                                        @Query("primary_release_date.lte") String toDate);
}