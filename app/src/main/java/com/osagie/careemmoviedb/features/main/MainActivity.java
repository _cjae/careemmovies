package com.osagie.careemmoviedb.features.main;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.osagie.careemmoviedb.R;
import com.osagie.careemmoviedb.features.details.DetailsActivity;
import com.osagie.careemmoviedb.features.filter.FilterActivity;
import com.osagie.careemmoviedb.features.main.adapter.MoviesAdapter;
import com.osagie.careemmoviedb.features.main.listener.OnClickItemListener;
import com.osagie.careemmoviedb.injector.Injector;
import com.osagie.careemmoviedb.model.Movie;
import com.osagie.careemmoviedb.model.MoviesWrapper;
import com.osagie.careemmoviedb.repository.AppRemoteDataStore;
import com.osagie.careemmoviedb.shared.BaseActivity;
import com.osagie.careemmoviedb.shared.RecyclerInsetsDecoration;
import com.osagie.careemmoviedb.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.osagie.careemmoviedb.utils.AppTags.EXTRAS_FROM;
import static com.osagie.careemmoviedb.utils.AppTags.EXTRAS_MOVIE;
import static com.osagie.careemmoviedb.utils.AppTags.EXTRAS_TO;
import static com.osagie.careemmoviedb.utils.AppTags.REQUEST_FILTER_CODE;
import static com.osagie.careemmoviedb.utils.ServerUtils.API_KEY;
import static com.osagie.careemmoviedb.utils.ServerUtils.SORT_LATEST;

public class MainActivity extends BaseActivity implements OnClickItemListener {

    @BindView(R.id.main_container)
    CoordinatorLayout mainContainer;

    @BindView(R.id.movies_swipe_refresh_view)
    SwipeRefreshLayout moviesSwipeRefreshView;

    @BindView(R.id.movies_rv)
    RecyclerView moviesRv;

    private List<Movie> data;
    private MoviesAdapter moviesAdapter;
    private AppRemoteDataStore appRemoteDataStore;
    private Subscription subscription;

    private int pageNumber = 0;
    private String fromDate = null;
    private String toDate = null;

    private Handler handler;
    private Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initViews();
        initRecyclerView();
        initListeners();

        // Initial Data Pull
        if (CommonUtils.isNetworkAvailable(this)) {
            doInitialDataPull();
        } else {
            doNoNetworkAvailable();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_filter) {
            doShowFilterDialog();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void doInitialDataPull() {
        pageNumber = 1;
        showProgress();
        doGetData();
    }

    private void doNoNetworkAvailable() {
        snackbar.show();
    }

    private void initViews() {
        handler = new Handler();

        appRemoteDataStore = (AppRemoteDataStore) Injector.provideRemoteAppRepository();
        moviesSwipeRefreshView.setOnRefreshListener(this::doRefreshData);

        snackbar = Snackbar
                .make(mainContainer, R.string.no_internet_connection, Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(R.string.retry_text), view -> {
                    doInitialDataPull();
                });

        // Changing message text color
        snackbar.setActionTextColor(Color.RED);

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
    }

    private void initRecyclerView() {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        moviesRv.setItemAnimator(new DefaultItemAnimator());
        moviesRv.setLayoutManager(mLayoutManager);
        moviesRv.addItemDecoration(new RecyclerInsetsDecoration(this));

        data = new ArrayList<>();
        moviesAdapter = new MoviesAdapter(this, data);
        moviesRv.setAdapter(moviesAdapter);

        moviesAdapter.initScrollListener(moviesRv);
    }

    private void initListeners () {
        moviesAdapter.setOnItemClickListener(this);
        moviesAdapter.setOnLoadMoreListener(() -> handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //add null , so the adapter will check view_type and show progress bar at bottom
                moviesAdapter.addLoadingView();

                doLoadMoreData();
            }
        }, 500));
    }

    private void doRefreshData() {
        pageNumber = 1;
        data.clear();
        doGetData();
    }

    private void doLoadMoreData() {
        if (CommonUtils.isNetworkAvailable(this)) {
            doGetData();
        } else {
            showPlainMessageToast(getString(R.string.no_internet_connection));
            doStopLoadMore(null);
        }
    }

    private void doGetData() {
        subscription = appRemoteDataStore.getMovies(SORT_LATEST, API_KEY, pageNumber,
                fromDate, toDate)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<MoviesWrapper>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        processError();
                    }

                    @Override
                    public void onNext(MoviesWrapper moviesWrapper) {
                        processResponse(moviesWrapper);
                    }
                });
    }

    private void processError() {
        showPlainMessageToast(getString(R.string.error_occurred));

        if (moviesAdapter.getLoaded()) {
            doStopLoadMore(null);
            return;
        }

        dismissProgress();
    }

    private void processResponse(MoviesWrapper moviesWrapper) {
        if (moviesWrapper == null) {
            processError();
            return;
        }

        if (moviesAdapter.getLoaded()) {
            doStopLoadMore(moviesWrapper.getResults());
            return;
        }

        pageNumber++;
        dismissProgress();
        data = moviesWrapper.getResults();
        moviesAdapter.setData(data);
    }

    private void showProgress() {

        moviesSwipeRefreshView.setRefreshing(true);
    }

    private void dismissProgress() {
        if (moviesSwipeRefreshView.isRefreshing()) {
            moviesSwipeRefreshView.setRefreshing(false);
        }
    }

    private void doStopLoadMore(List<Movie> results) {
        handler.postDelayed(() -> {
            //   remove progress item
            moviesAdapter.removeLoadingView();

            if (results != null && !results.isEmpty()) {
                pageNumber++;
                moviesAdapter.updateData(results);
            }

            moviesAdapter.setLoaded();

        }, 2000);
    }

    private void doShowFilterDialog() {
        Intent intent = new Intent(this, FilterActivity.class);
        startActivityForResult(intent, REQUEST_FILTER_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_FILTER_CODE) {
            if(resultCode == Activity.RESULT_OK){
                Bundle bundle = data.getExtras();
                processResult(bundle);
            }
        }
    }

    private void processResult(Bundle bundle) {
        if (bundle == null) {
            showPlainMessageToast(getString(R.string.error_filter));
            return;
        }

        fromDate = null;
        toDate = null;

        String from = bundle.getString(EXTRAS_FROM);
        String to = bundle.getString(EXTRAS_TO);

        if (!TextUtils.isEmpty(from) && !from.equals(getString(R.string.select_from_date))) {
            fromDate = from;
        }

        if (!TextUtils.isEmpty(to) && !to.equals(getString(R.string.select_to_date))) {
            toDate = to;
        }

        doInitialDataPull();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    @Override
    public void onClickMovieItem(Movie movie) {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra(EXTRAS_MOVIE, movie);
        startActivity(intent);
    }
}