package com.osagie.careemmoviedb.features.main.listener;

import com.osagie.careemmoviedb.model.Movie;

public interface OnClickItemListener {
    void onClickMovieItem(Movie movie);
}
