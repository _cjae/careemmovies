package com.osagie.careemmoviedb.features.details;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.osagie.careemmoviedb.R;
import com.osagie.careemmoviedb.model.Movie;
import com.osagie.careemmoviedb.shared.BaseActivity;
import com.osagie.careemmoviedb.utils.CommonUtils;

import butterknife.BindView;

import static com.osagie.careemmoviedb.utils.AppTags.EXTRAS_MOVIE;
import static com.osagie.careemmoviedb.utils.ServerUtils.BASE_COVER_URL;

public class DetailsActivity extends BaseActivity {

    @BindView(R.id.activity_detail_title)
    TextView activity_detail_title;

    @BindView(R.id.genre_vw)
    TextView genre_vw;

    @BindView(R.id.release_date)
    TextView date_title;

    @BindView(R.id.language_vw)
    TextView language_vw;

    @BindView(R.id.rating_vw)
    TextView rating_vw;

    @BindView(R.id.synopsis_tv)
    TextView synopsis_tv;

    @BindView(R.id.item_movie_cover)
    ImageView itemMovieCover;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(getSupportActionBar()!= null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        setUpViews();
    }

    private void setUpViews() {
        Intent intent = getIntent();

        if(intent.hasExtra(EXTRAS_MOVIE)) {
            Movie movie = intent.getParcelableExtra(EXTRAS_MOVIE);

            fillViews(movie);

            return;
        }

        finish();
    }

    private void fillViews(Movie movie) {
        activity_detail_title.setText(movie.getOriginal_title());
        synopsis_tv.setText(movie.getOverview());

        String dateString = String.format(getString(R.string.release_date),
                movie.getRelease_date());
        date_title.setText(dateString);

        String ratingString = String.format(getString(R.string.rating_text),
                String.valueOf(movie.getVote_average()));
        rating_vw.setText(ratingString);

        String languageString = String.format(getString(R.string.language),
                movie.getOriginal_language().toUpperCase());
        language_vw.setText(languageString);

        String genreString = CommonUtils.changeGenreToString(movie.getGenre_ids());
        genre_vw.setText(genreString);

        showCoverImage(movie.getBackdrop_path());
    }

    private void showCoverImage(String path) {
        Glide.with(this)
                .load(BASE_COVER_URL + path)
                .into(itemMovieCover);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
