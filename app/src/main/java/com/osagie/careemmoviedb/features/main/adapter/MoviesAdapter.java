package com.osagie.careemmoviedb.features.main.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.osagie.careemmoviedb.R;
import com.osagie.careemmoviedb.features.main.listener.OnClickItemListener;
import com.osagie.careemmoviedb.features.main.listener.OnLoadMoreListener;
import com.osagie.careemmoviedb.model.Movie;
import com.osagie.careemmoviedb.utils.AppTags;
import com.osagie.careemmoviedb.utils.ServerUtils;

import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter {

    private List<Movie> data;
    private Context mContext;

    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;

    // Listeners
    private OnClickItemListener onClickItemListener;
    private OnLoadMoreListener onLoadMoreListener;

    public MoviesAdapter(Context mContext, List<Movie> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == AppTags.CARD_MOVIE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.movie_item, parent, false);

            vh = new MoviesHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progress_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MoviesHolder) {

            Movie item = data.get(position);
            MoviesHolder movieHolder = (MoviesHolder) holder;

            String title = item.getTitle();
            String posterPath = item.getBackdrop_path();

            if (!TextUtils.isEmpty(title)) {
                movieHolder.itemTitle.setText(title);
            }

            if (!TextUtils.isEmpty(posterPath)) {
                Glide.with(mContext)
                        .load(ServerUtils.BASE_COVER_URL + posterPath)
                        .into(movieHolder.poster);
            }
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position) != null ? AppTags.CARD_MOVIE : AppTags.CARD_LOADING;
    }

    // init methods
    public void initScrollListener(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setOnItemClickListener(OnClickItemListener onClickItemListener) {
        this.onClickItemListener = onClickItemListener;
    }

    // Get and Set Properties Methods
    public boolean getLoaded() {
        return loading;
    }

    public void setLoaded() {
        loading = false;
    }

    public void setData(List<Movie> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void updateData(List<Movie> data) {
        this.data.addAll(data);
        notifyItemInserted(data.size());
    }

    public void addLoadingView() {
        data.add(null);
        notifyItemInserted(data.size() - 1);
    }

    public void removeLoadingView() {
        data.remove(data.size() - 1);
        notifyItemRemoved(data.size());
    }

    // View Holder Classes
    class MoviesHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {

        ImageView poster;
        TextView itemTitle;

        MoviesHolder(View itemView) {
            super(itemView);

            poster = itemView.findViewById(R.id.movie_poster);
            itemTitle = itemView.findViewById(R.id.item_movie_title);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            Movie movie = data.get(position);

            if (onClickItemListener != null) {
                onClickItemListener.onClickMovieItem(movie);
            }
        }
    }

    class ProgressViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;

        ProgressViewHolder(View v) {
            super(v);
            progressBar = v.findViewById(R.id.progressBar);
        }
    }
}