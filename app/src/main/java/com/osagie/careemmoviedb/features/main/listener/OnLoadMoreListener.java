package com.osagie.careemmoviedb.features.main.listener;

public interface OnLoadMoreListener {
    void onLoadMore();
}