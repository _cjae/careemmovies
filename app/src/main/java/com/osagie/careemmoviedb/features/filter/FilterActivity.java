package com.osagie.careemmoviedb.features.filter;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import com.osagie.careemmoviedb.R;
import com.osagie.careemmoviedb.shared.BaseActivity;

import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;

import static com.osagie.careemmoviedb.utils.AppTags.EXTRAS_FROM;
import static com.osagie.careemmoviedb.utils.AppTags.EXTRAS_TO;

public class FilterActivity extends BaseActivity {

    @BindView(R.id.from_date_tv)
    TextView fromDateTv;

    @BindView(R.id.to_date_tv)
    TextView toDateTv;

    private int fromYear = 0;
    private TextView selectedTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(getSupportActionBar()!= null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void selectFromDate(View view) {
        selectedTextView = fromDateTv;
        showDatePickerDialog();
    }

    public void selectToDate(View view) {
        selectedTextView = toDateTv;
        showDatePickerDialog();
    }

    private void showDatePickerDialog() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                (view, year, monthOfYear, dayOfMonth) -> fillTextView(year, monthOfYear + 1,
                        dayOfMonth), mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void fillTextView(int year, int month, int dayOfMonth) {
        if (selectedTextView == null) {
            return;
        }

        if (selectedTextView == fromDateTv) {
            fromYear = year;
            fromDateTv.setText(String.format(Locale.US, "%d-%d-%d", year, month,
                    dayOfMonth));

            return;
        }

        if (selectedTextView == toDateTv) {
            if (year > fromYear) {
                toDateTv.setText(String.format(Locale.US, "%d-%d-%d", year, month,
                        dayOfMonth));
            } else {
                showPlainMessageToast(getString(R.string.calender_year_error));
            }
        }
    }

    public void doFilter(View view) {
        String from = fromDateTv.getText().toString();
        String to = toDateTv.getText().toString();

        Bundle bundle = new Bundle();
        bundle.putString(EXTRAS_FROM, from);
        bundle.putString(EXTRAS_TO, to);

        Intent returnIntent = new Intent();
        returnIntent.putExtras(bundle);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}