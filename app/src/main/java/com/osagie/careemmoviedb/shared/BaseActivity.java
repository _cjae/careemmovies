package com.osagie.careemmoviedb.shared;

import android.annotation.SuppressLint;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import butterknife.ButterKnife;

@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }

    public void showPlainMessageToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public void doDebugLog(String log) {
        Log.d(this.getClass().getSimpleName(), "doDebugLog: " + log);
    }

    public void doErrorLog(Throwable e) {
        Log.e(this.getClass().getSimpleName(), "doErrorLog: " + e.getMessage(), e);
    }
}