package com.osagie.careemmoviedb.utils;

public class ServerUtils {

    public static final String BASE_URL = "http://api.themoviedb.org/3/";

    public static final String BASE_IMAGE_URL = "http://image.tmdb.org/t/p/w185";
    public static final String BASE_COVER_URL = "http://image.tmdb.org/t/p/w500";

    public static final String SORT_LATEST = "popularity.desc";
    public static final String API_KEY = "deeb47dd57bfd8343cbb61348eeb0c87";
}
