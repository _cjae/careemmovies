package com.osagie.careemmoviedb.utils;

public class AppTags {

    public static String EXTRAS_MOVIE = "movie";
    public static String EXTRAS_FROM = "from";
    public static String EXTRAS_TO = "to";

    public static int REQUEST_FILTER_CODE = 122;

    public static int CARD_MOVIE = 1;
    public static int CARD_LOADING = 2;
}
