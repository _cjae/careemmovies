package com.osagie.careemmoviedb.repository;

import com.osagie.careemmoviedb.model.MoviesWrapper;

import rx.Observable;

public interface AppDataStore {

    Observable<MoviesWrapper> getMovies(String sort_type, String api_key, int page,
                                        String fromDate, String toDate);
}