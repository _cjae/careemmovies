package com.osagie.careemmoviedb.repository;

import com.osagie.careemmoviedb.injector.Injector;
import com.osagie.careemmoviedb.model.MoviesWrapper;
import com.osagie.careemmoviedb.network.ServiceGenerator;

import rx.Observable;

public class AppRemoteDataStore implements AppDataStore {

    private ServiceGenerator mServiceGenerator;

    public AppRemoteDataStore() {
        mServiceGenerator = Injector.getServiceGenerator();
    }

    @Override
    public Observable<MoviesWrapper> getMovies(String sort_type, String api_key, int page,
                                               String fromDate, String toDate) {
        return mServiceGenerator.getMovies(sort_type, api_key, page, fromDate, toDate);
    }
}